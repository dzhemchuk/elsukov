$(document).ready(function(){
    $(".feedback-item__text").each(function(){
        if($(this).height() >= 220){
            $(this).parents(".feedback-item").addClass("feedback-item_arrow");
        }
    });

    function pushBars(min, max){
        $(".questions-animation-bar").each(function(){
            let random = Math.floor(Math.random() * (max - min + 1) + min);
            $(this).css("bottom", random + "px");
        });
    }

    let min = -130;
    let max = 130;
    if($(window).width() < 1200){
        min = -65;
        max = 65;
    }
    pushBars(min, max);

    window.setInterval(function(){
        pushBars(min, max);
    }, 750);

    $("button.cta").click(function(){
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".submit .submit__content").offset().top - 100
        }, 2000);
    });
});