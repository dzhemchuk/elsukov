$(document).ready(function(){
    let currentQuestion = 0;
    const allQuestion = $(".form-question").length - 1;
    $(".form-question").eq(0).addClass("form-question_active");

    function setBar(current){
        let totalAmount = allQuestion + 1;
        let width = current * 100 / totalAmount;
        $(".form-progress__bar").css("width", width+"%");
    }

    setBar(1);
    
    let input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#phone-error");
    
    let iti = window.intlTelInput(input, {
        initialCountry: "ru",
        utilsScript: "utils.js"
    });

    let errorMap = ["Неправильный номер", "Неправильный код страны", "Слишком короткий", "Слишком длинный", "Неправильный номер"];

    let reset = function() {
        $(".form-input__error").hide();
        $("#phone").removeClass("form-input__input_ok");
        $(".form-submit").removeClass("form-submit_error");
      };

    input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
          if (iti.isValidNumber()) {
            $("#phone").addClass("form-input__input_ok");
          } else {
            $("#phone-error").css("display", "block");
            $(".form-submit").addClass("form-submit_error");
            let errorCode = iti.getValidationError();
            if(errorCode == -99){
                $("#phone-error").text("Недопустимые символы");
            }
            else{
                $("#phone-error").text(errorMap[errorCode]);
            }
          }
        }
      });

    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);

    function ValidateInputs(validateInput){
        reset();

        if(validateInput.attr("name") == "name"){
            if(validateInput.val().length < 3){
                $("input[name='name']").siblings(".form-input__error").css("display", "block");
                $(".form-submit").addClass("form-submit_error");
            }
            else{
                $("input[name='name']").addClass("form-input__input_ok");
            }
        }

        if(validateInput.attr("name") == "email"){
            let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(regex.test(validateInput.val())){
                $("input[name='email']").addClass("form-input__input_ok");
            }
            else{
                $("input[name='email']").siblings(".form-input__error").css("display", "block");
                $(".form-submit").addClass("form-submit_error");
            }
        }
    }

    $(".form-input__input").change(function(){
        ValidateInputs($(this));
    });

    $(".form-input__input").keyup(function(){
        ValidateInputs($(this));
    });

    $(".form-wrapper__content").submit(function(e){
        e.preventDefault();
        
        if($("input[name='name']").val().length == 0){
            $("input[name='name']").siblings(".form-input__error").css("display", "block");
            $(".form-submit").addClass("form-submit_error");
        }
        
        if($("input[name='email']").val().length == 0){
            $("input[name='email']").siblings(".form-input__error").css("display", "block");
            $(".form-submit").addClass("form-submit_error");
        }
        
        if($("input[name='tel']").val().length == 0){
            $("#phone-error").css("display", "block");
            $("#phone-error").text("Введите телефон");
            $(".form-submit").addClass("form-submit_error");
        }

        if(!$(".form-submit").hasClass("form-submit_error")){
            $.ajax({
                url: window.location.href,
                data: $(".form-wrapper__content").serialize(),
                type: "post",
                beforeSend: function() {
                    $(".form-submit").addClass("form-submit_error");
                },
                success: function(data){
                    if(data == "OK"){
                        $(".content").hide();
                        $(".success").show();
                    }
                    else{
                        console.log(data);
                        alert("Произошла ошибка. Попробуйте позже");
                    }
                },
                error: function(){
                    alert("Произошла ошибка. Попробуйте позже");
                }
              })
        }
    });

    $(".form-btn_next").click(function(){
        if($(".form-question_active input:checked").val()){
            let nextQuestion = currentQuestion + 1;
            if(nextQuestion <= allQuestion){
                $(".form-btn_prev").css("display", "block");
                $(".form-question").removeClass("form-question_active");
                $(".form-question").eq(nextQuestion).addClass("form-question_active");
                currentQuestion = nextQuestion;
                $("#current-question").text(currentQuestion + 1);
                if(nextQuestion == allQuestion){
                    $(".form-btns").hide();
                    $(".questions-counter").hide();
                    $(".form-question").eq(nextQuestion).css("margin", "0");
                }
                setBar(nextQuestion + 1);
            }
        }
    })

    $(".form-btn_prev").click(function(){
        let prevQuestion = currentQuestion - 1;
        
        if(prevQuestion >= 0){
            $(".form-question").removeClass("form-question_active");
            $(".form-question").eq(prevQuestion).addClass("form-question_active");
            currentQuestion = prevQuestion;
            $("#current-question").text(prevQuestion + 1);
            if(prevQuestion - 1 < 0){
                $(".form-btn_prev").hide();
            }
            setBar(prevQuestion + 1);
        }
    })
});