<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
require_once('PHPMailer.php');
require_once('SMTP.php');
if(!empty($_POST)){
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP(); 
    $mail->Host       = 'mail.elsukovrv.ru';   
    $mail->SMTPAuth   = true;
    $mail->Username   = 'no-reply@elsukovrv.ru';
    $mail->Password   = 'iS0nK1fP6e';
    $mail->Port       = 465;
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    $mail->SMTPDebug  = 0;  
    
    $mail->setFrom('no-reply@elsukovrv.ru');
    $mail->addAddress('ilap2508@gmail.com');
    $mail->addReplyTo('no-reply@elsukovrv.ru');
    
    $mail->isHTML(true); 
    $mail->Subject = 'Получена заявка на консультацию';
    
    $message = '';
    $messageHtml = '';

    foreach($_POST as $name => $value){
        if($name == "name"){
            $message .= "Имя: $value \n";
            $messageHtml .= "Имя: $value <br>";
        }
        elseif($name == "email"){
            $message .= "Почта: $value \n";
            $messageHtml .= "Почта: $value <br>";
        }
        elseif($name == "tel"){
            $message .= "Телефон: $value \n";
            $messageHtml .= "Телефон: $value <br>";
        }
        else{
            $message .= "$value \n";
            $messageHtml .= "$value <br>";
        }
    }
    
    $mail->Body    = $messageHtml;
    $mail->AltBody = $message;
    $mail->send();

	die("OK");
}

$questions = [
	[
		"question" => "Для чего вам аудит?",
		"answers" => ["Хочу выйти из долгов", "Хочу научиться копить деньги", "Хочу научиться инвестировать, но не знаю куда", "Уже инвестирую, но хочу доходность выше", "Другое"]
	],
	[
		"question" => "Ваш вид деятельности?",
		"answers" => ["Наемный сотрудник", "Руководящая должность", "Фриланс", "Собственник бизнеса"]
	],
	[
		"question" => "Ваш ежемесячный доход",
		"answers" => ["от 0 до 50 000 рублей", "от 50 000 до 100 000 рублей", "от 100 000 рублей до 150 000 рублей", "от 150 000 рублей до 300 000 рублей", "более 300 000 рублей"]
	],
	[
		"question" => "Соотношение Ваших доходов и расходов",
		"answers" => ["Расходы больше доходов", "Доходы равны расходам", "Доходы больше расходов"]
	],
	[
		"question" => "Сколько процентов от своих доходов Вы откладываете ежемесячно?",
		"answers" => ["Не откладываю", "5%", "10%", "20%", "30%", "Более 30%"]
	],
	[
		"question" => "Сумма Ваших накоплений на данный момент?",
		"answers" => ["Менее месячного расхода", "1-2 месячных расхода", "3-5 месячных расхода", "Более 6 месячных расходов"]
	],
	[
		"question" => "Какая текущая доходность Ваших инвестиций",
		"answers" => ["Не инвестирую", "До 5% годовых", "5-10% годовых", "10-20% годовых", "20-30% годовых", "Более 30% годовых"]
	],
	[
		"question" => "Какие у Вас долговые обязательства?",
		"answers" => ["Кредитная карта", "Потребительский кредит", "Частные займы", "Микрокредиты", "Ипотека", "Кредиты, как рычаг для инвестиций", "Нет долгов"]
	]
];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Роман Елсуков – Форма | Финансовый консультант</title>
	<meta name="description" content="Составление персонального финансового плана">
	<meta name="keywords" content="персональный финансовый план, финансовый консультант, пассивный доход">

	<!-- OG Tags -->
	<meta property="og:title" content="Роман Елсуков | Финансовый консультант">
	<meta property="og:description" content="Составление персонального финансового плана">
	<meta property="og:type" content="article">
	<meta property="og:image" content="https://sitename.ru/img/preview.jpg">
	<meta property="og:site_name" content="Роман Елсуков | Финансовый консультант">

	<!-- Favicons -->
	<meta name="theme-color" content="#111111">

	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.googleapis.com"> 
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> 
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600&display=swap" rel="stylesheet">

	<!-- Styling -->
	<link rel="stylesheet" href="css/intlTelInput.min.css">
	<link rel="stylesheet" href="css/form.css">
</head>
<body>
    <section class="content">
        <div class="container">
            <h1 class="content-title">
                Анкета на бесплатный аудит по личным финансам
            </h1>
            <p class="content-subtitle">
                Для экономии Вашего времени просим заполнить короткую анкету, которая позволит нам понять Вашу текущую ситуацию и предоставить Вам корректные рекомендации. Важно! Заполнять анкету нужно честно, так как от этого зависят рекомендации для Вас.
            </p>
            <div class="form-wrapper">
                <form action="" class="form-wrapper__content">
                    <div class="form-progress">
                        <div class="form-progress__bar"></div>
                    </div>
                    <div class="questions-counter">
                        <span class="questions-counter__text">
                            Вопрос
                        </span>
                        <span class="questions-counter__number" id="current-question">
                            1
                        </span>
                        <span class="questions-counter__text">
                            из
                        </span>
                        <span class="questions-counter__number" id="all-question">
                            <?= count($questions) + 1 ?>
                        </span>
                    </div>

                    <?php
                    $i = 1;
                    foreach ($questions as $question) {
                    ?>

                    <div class="form-question">
                        <p class="question-title">
                            <?= $question["question"] ?>
                        </p>
                        <ul class="options">

                        	<?php
                        	$j = 1;
                        	foreach ($question["answers"] as $answer) {
                        	?>
                            <li class="option">
                                <input type="radio" id="q<?= $i ?>-a<?= $j ?>" name="q<?= $i ?>" class="option-input" value="<?= $question["question"] . ": " . $answer ?>">
                                <label for="q<?= $i ?>-a<?= $j ?>" class="option-label">
                                    <span class="option-label__radio"></span>
                                    <p class="option-label__text">
                                        <?= $answer ?>
                                    </p>
                                </label>
                            </li>
                        	<?php
                        	$j++;
                        	}
                        	?>

                        </ul>
                    </div>

                    <?php 
                    $i++;
                    }
                    ?>

                    <div class="form-question">
                        <p class="questio-title_big">
                            Укажите ваши контакты
                            <span class="questio-title_big__subtitle">
                                и мы вышлем материал:
                            </span>
                        </p>
                        <div class="form-input">
                            <span class="form-input__title">
                                Имя
                            </span>
                            <input type="text" name="name" class="form-input__input" placeholder="Александр Иванов">
                            <span class="form-input__error">
                                Необходимо ввести имя
                            </span>
                        </div>
                        <div class="form-input">
                            <span class="form-input__title">
                                E-Mail
                            </span>
                            <input type="text" name="email" class="form-input__input" placeholder="example@mail.ru">
                            <span class="form-input__error">
                                Необходимо почту
                            </span>
                        </div>
                        <div class="form-input">
                            <span class="form-input__title">
                                Номер телефона
                            </span>
                            <input type="text" name="tel" id="phone" class="form-input__input" placeholder="+7 912 345-67-89 ">
                            <span class="form-input__error" id="phone-error">
                                Слишком короткий номер
                            </span>
                        </div>
                        <button type="submit" class="form-submit">
                            продолжить
                        </button>
                        <p class="form-agreement">
                            Я согласен на обработку моих персональных данных в соответствии с
                            <a href="#" class="form-agreement__a">Условиями</a>.
                        </p>
                    </div>
                    <div class="form-btns">
                        <button type="button" class="form-btn form-btn_prev">
                            Назад
                        </button>
                        <button type="button" class="form-btn form-btn_next">
                            далее
                        </button>
                    </div>
                </form>
                <div class="form-wrapper__image">
                    <img src="img/form-bg.png">
                </div>
            </div>
        </div>
    </section>
    <div class="success">
        <h2 class="success__title">
            Благодарю!
        </h2>
        <p class="success__p">
            Ваша заявка принята.<br>
            Она будет рассмотрена в ближайшее время.
        </p>
    </div>
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/intlTelInput.min.js"></script>
    <script src="js/form.js"></script>
</body>
</html>